import logging
import socket
from Client import Client
from NetworkServer import NetworkServer

# Constant Variables:
__author__    = 'Shachar'
MINIMUM_TRY   = 0
LOGGING_LEVEL = logging.DEBUG


class Server(object):
    # Constant Variables:
    Stop               = "STOP"
    log_file_type      = ".log"
    get_current_try    = "TRY"
    no_solution        = "No-Solution"
    done_calculattion  = "DONE"

    def __init__(self, md5_string, length, ip, port, minimum_try=MINIMUM_TRY, logging_level=LOGGING_LEVEL, path_file="Server"):
        # Utilities variables:
        self.log            = Client.create_log(path_file + Server.log_file_type, logging_level)
        # Network variables:
        self.network_server = NetworkServer(ip, port, self.log)
        self.run            = False
        # MD5-Cracking variables:
        self.md5_string     = md5_string.lower()
        self.length         = length
        self.maximum_try    = int('1' + '0' * self.length) - 1
        self.minimum_try    = minimum_try
        self.current_try    = self.minimum_try

    # ------------Data-Respond Functions------------
    def add_client_and_send_information(self):
        new_client_socket = self.network_server.add_client_and_get_him()
        self.network_server.add_answer(new_client_socket, self.md5_string + " " + str(self.length))  # Send to new client information to start calculate
        self.log.info("new Client added : " + str(new_client_socket))

    def try_to_increase_current_try(self, current_socket):
        self.log.debug("current_try is, " + str(self.current_try) + " max is " + str(self.maximum_try))
        if self.current_try <= self.maximum_try:
            self.network_server.add_answer(current_socket, str(self.current_try))
            self.current_try += 1   # next possibility
            self.log.debug("Increasing current_try to " + str(self.current_try))
        else:
            self.log.debug(str(current_socket) + " says No Solution")
            return Server.no_solution   # No Solution

    def stop_calculation(self, current_socket):
        self.log.debug("Send to all clients to stop ")
        self.network_server.add_answer(current_socket, Server.Stop, True)   # Stop all others clients
        self.run = False

    # ------------Main Loop------------
    def start_online_calculation(self):
        self.run = True
        self.network_server.open()
        self.log.info("Server opened!")
        while self.run or self.network_server.messages_to_send == []:
            self.log.info(str(self.current_try))
            rlist, wlist, xlist = self.network_server.get_clients_status()
            for current_socket in rlist:
                if self.network_server.is_sendable(current_socket):
                    self.add_client_and_send_information()
                else:
                    try:
                        data = self.network_server.get_data(current_socket)
                    except socket.error:     # Client quited by error
                        data = ""

                    self.log.debug("got data : \"" + data + "\" from: " + str(current_socket))

                    if data == "":
                        self.network_server.delete_client(current_socket)

                    elif data.upper() == Server.get_current_try:
                        if self.try_to_increase_current_try(current_socket) == Server.no_solution:
                            return Server.no_solution

                    elif Server.done_calculattion in data.upper():
                        answer = data.split()[-1]
                        self.log.info("Done, Answer is: " + answer)
                        self.stop_calculation(current_socket)
                    else:
                        self.log.error("Error: Incorrect answer >> " + data)    # Incorrect answer

            try:
                self.network_server.send_answers(wlist)
            except socket.error:     # Client quited by error
                continue

        return answer   # Return solution


#------------Main------------
def main():
    server = Server(md5_string="EC9C0F7EDCC18A98B1F31853B1813301", length=10, ip="0.0.0.0", port=8820, minimum_try=0) # Fast one: minimum_try = 3735921559
    server.start_online_calculation()
    raw_input("Press <Enter> to Exit...")

if __name__ == "__main__":
    main()