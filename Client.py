import md5
import multiprocessing
import socket
import threading
import logging  # This module is Thread-Safe
import sys

# Constant Variables:
__author__    = 'Shachar'
LOGGING_LEVEL = logging.DEBUG


class Client(object):
    # Constant Variables:
    stop            = "STOP"
    log_file_type   = ".log"
    receive_buffer  = 1024
    get_current_try = "TRY"
    done            = "Done"

    def __init__(self, ip, port, thread_num="", logging_level=LOGGING_LEVEL, path_file="log"):
        # Network variables:
        self.socket         = socket.socket()
        self.ip             = ip
        self.port           = port
        # MD5-Cracking variables:
        self.md5_string     = None
        self.length         = None
        # Utilities variables:
        self.thread_string  = " Thread_num-" + str(thread_num)
        #self.lock           = threading.Lock()
        self.log            = self.create_log(path_file + self.thread_string + Client.log_file_type, logging_level)

    @staticmethod
    def create_log(filename, file_logging_level, screen_logging_level=logging.INFO):
        log            = logging.getLogger(__name__)
        file_handler   = logging.FileHandler(filename=filename, mode='a')
        file_handler.setLevel(file_logging_level)
        formatter      = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        log.addHandler(file_handler)

        stream_handle = logging.StreamHandler(sys.stdout)
        stream_handle.setLevel(screen_logging_level)
        formatter     = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stream_handle.setFormatter(formatter)
        log.addHandler(stream_handle)

        log.setLevel(file_logging_level)
        return log

    # ------------Network-Functions------------
    def connect(self):
        self.log.debug("Trying connect to server" + self.thread_string)
        self.socket.connect((self.ip, self.port))
        self.log.info("connected" + self.thread_string)

    def __receive(self, debug=True):
        data = self.socket.recv(Client.receive_buffer)
        if debug:
            self.log.debug("got : \"" + data + "\"" + self.thread_string)
        return data

    def __send(self, data):
        self.socket.send(data)
        self.log.debug("send : \"" + data + "\"" + self.thread_string)

    def receive_md5(self):
        data                         = self.__receive(debug=False)
        self.md5_string, self.length = data.split()     # Data should be receive in this pattern
        self.length                  = int(self.length)
        self.log.debug("got md5, length: " + self.md5_string + ", " + str(self.length) + self.thread_string)

    # ------------MD5-Functions------------
    def md5_cracking(self):
        running = True
        while running:

            #self.lock.acquire()
            self.__send(Client.get_current_try)
            current_try = self.__receive()
            #self.lock.release()
            if Client.stop in current_try:
                break

            if len(current_try) < self.length:
                current_try = ('0' * (self.length - len(current_try))) + current_try  # Adding '0' on left
            md5_string = self.string_to_md5(current_try)

            self.log.debug("Checking if md5 == " + md5_string + self.thread_string)
            if md5_string == self.md5_string:
                #self.lock.acquire()
                self.__send(Client.done + " " + str(current_try))
                #self.lock.release()
                running = False

    @staticmethod
    def string_to_md5(current_try):
        md5_object = md5.new()
        md5_object.update(current_try)
        md5_string = md5_object.hexdigest()
        return md5_string

    # ------------Start------------
    def start_client(self):
        self.connect()
        self.receive_md5()
        self.md5_cracking()


# ------------Threads-Functions------------
def run_threads(server_ip, server_port):
    filename      = "main_log.log"
    logging_level = logging.DEBUG
    main_log      = Client.create_log(filename, logging_level)
    threads       = []
    for i in xrange(multiprocessing.cpu_count()):
        client = Client(server_ip, server_port, i+1)
        thread = threading.Thread(target=client.start_client)
        threads.append(thread)
        thread.start()

    main_log.info("start Threads")
    main_log.debug("Waiting for Threads to finish")
    for thread in threads:
        thread.join()   # Waiting until all threads will finish
    main_log.debug("Threads finished")


def has_alive(threads):
    for thread in threads:
        if thread.is_alive():
            return True
    return False


#------------Main------------
def main():
    main_ip   = "127.0.0.1"
    main_port = 8820
    run_threads(main_ip, main_port)
    raw_input("Press <Enter> to Exit...")

if __name__ == "__main__":
    main()

