import socket
import select

CLIENTS_LISTEN = 5
RECEIVE_SIZE   = 1024


class NetworkServer(object):
    def __init__(self, ip, port, log=None):
        self.ip                  = ip
        self.port                = port
        self.messages_to_send    = []
        self.open_client_sockets = []
        self.server_socket       = socket.socket()
        self.log                 = log

    def open(self):
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(CLIENTS_LISTEN)

    def get_clients_status(self):
        return select.select( [self.server_socket] + self.open_client_sockets, self.open_client_sockets, [] )

    def is_sendable(self, current_socket):
        return current_socket is self.server_socket

    def add_client_and_get_him(self):
        (new_socket, address) = self.server_socket.accept()
        self.open_client_sockets.append(new_socket)
        return new_socket

    def get_data(self, current_socket):
        return current_socket.recv(RECEIVE_SIZE)

    def delete_client(self, current_socket):
        self.open_client_sockets.remove(current_socket)

    def add_answer(self, client_socket, data, send_all=False):
        self.messages_to_send.append((client_socket, data, send_all))

    def send_answers(self, wlist):
        for message in self.messages_to_send:
            (client_socket, data, send_all) = message
            if self.log is not None:
                self.log.debug("Try to send " + data + " to " + str(client_socket))     # Debug
            sent = False
            for recv_socket in wlist:
                if send_all:
                    if recv_socket != client_socket:
                        recv_socket.send(data)
                        if self.log is not None:
                            self.log.debug("send " + data + " to " + str(recv_socket))     # Debug
                    sent = True

                elif recv_socket == client_socket:
                    if self.log is not None:
                        self.log.debug("send " + data + " to " + str(recv_socket))     # Debug
                    recv_socket.send(data)
                    sent = True
            if sent:
                self.messages_to_send.remove(message)

if __name__ == "__main__":
    RUN    = True
    IP     = "127.0.0.1"
    PORT   =  8820
    server = NetworkServer(IP, PORT)
    server.open()
    while RUN:
        rlist, wlist, xlist = server.get_clients_status()
        for current_socket in rlist:
            if server.is_sendable(current_socket):
                server.add_client_and_get_him()
            else:
                data = server.get_data(current_socket)
                if data == "":
                    server.delete_client(current_socket)
                else:
                    print "got : "          # Debug
                    print "<" + data + ">"  # Debug
                    server.add_answer(current_socket, "Hello " + data)
        server.send_answers(wlist)